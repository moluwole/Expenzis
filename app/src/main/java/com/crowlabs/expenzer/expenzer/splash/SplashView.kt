package com.crowlabs.expenzer.expenzer.splash

interface SplashView {
    interface View {
        fun launchHome()
    }

    interface Action {
        fun startTimer()
    }
}