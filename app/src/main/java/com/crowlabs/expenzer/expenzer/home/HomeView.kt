package com.crowlabs.expenzer.expenzer.home

interface HomeView {
    interface  View {
        fun showErrorMessage(error: String)

        fun showSuccessMessage(message: String)

        fun showInfo(message: String)
    }

    interface Action {
        fun loadTopics()
    }

}