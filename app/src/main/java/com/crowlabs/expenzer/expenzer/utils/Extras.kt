package com.crowlabs.expenzer.expenzer.utils

object Extras {
    val REQUEST_CODE = 1

    val TITLE_TABLE = "title"
    val DETAILS_TABLE = "details"

    //Columns
    val ID = "_id"
    val TITLE = "title"
    val DESCRIPTION = "description"
    val MONTH = "month"
    val DETAILS = "details_text"
    val AMOUNT = "amount"
    val DATE = "date"
    val CATEGORY = "category"

}